//
//  DeadReckoningModel.swift
//  DEAD_RECKONING_iOS_thesis_bme_2017
//
//  Created by Benedek Varga on 2017. 04. 29..
//  Copyright © 2017. Benedek Varga. All rights reserved.
//

import Foundation

class DeadReckoningModel: NSObject, NSCoding {
    var title: String = ""
    var isThresholdDynamic: Bool = false
    
    var userHeigth: Int64 = 0
    var isUserMale: Bool = true
    var pedometerStepCount: Int64 = 0
    
    //raw data from sensor
    var sensorPoints: [SensorDataPoint] = []
    
    //array helping the determination of steps: all positive peaks between to negative peaks
    var stepPointsBetweenZeros: [SensorDataPoint] = []
    //actual points of steps
    var calculatedStepPoints: [SensorDataPoint] = []
    
    //string array to display data in cells
    var stepPointsDataToPrint: [String] = []
    
    
    var THRESHOLD = 0.14
    var TIMEDIFFERENCE = 0.5
    var previousStepPointTime = 0.0
    
    override init() {
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(self.title, forKey: "title")
        aCoder.encode(self.isThresholdDynamic, forKey: "isThresholdDynamic")
        aCoder.encode(self.userHeigth, forKey: "userHeigth")
        aCoder.encode(self.isUserMale, forKey: "isUserMale")
        aCoder.encode(self.sensorPoints, forKey: "sensorPoints")
        aCoder.encode(self.stepPointsBetweenZeros, forKey: "stepPointsBetweenZeros")
        aCoder.encode(self.calculatedStepPoints, forKey: "calculatedStepPoints")
        aCoder.encode(self.stepPointsDataToPrint, forKey: "stepPointsDataToPrint")
        aCoder.encode(self.THRESHOLD, forKey: "THRESHOLD")
        aCoder.encode(self.TIMEDIFFERENCE, forKey: "TIMEDIFFERENCE")
        aCoder.encode(self.previousStepPointTime, forKey: "previousStepPointTime")
        aCoder.encode(self.pedometerStepCount, forKey: "pedometerStepCount")
    }
    
    required init?(coder aDecoder: NSCoder) {
        self.title = aDecoder.decodeObject(forKey: "title") as! String
        self.isThresholdDynamic = aDecoder.decodeBool(forKey: "isThresholdDynamic")
        self.userHeigth = aDecoder.decodeInt64(forKey: "userHeigth")
        self.isUserMale = aDecoder.decodeBool(forKey: "isUserMale")
        self.sensorPoints = aDecoder.decodeObject(forKey: "sensorPoints") as! [SensorDataPoint]
        self.stepPointsBetweenZeros = aDecoder.decodeObject(forKey: "stepPointsBetweenZeros") as! [SensorDataPoint]
        self.calculatedStepPoints = aDecoder.decodeObject(forKey: "calculatedStepPoints") as! [SensorDataPoint]
        self.stepPointsDataToPrint = aDecoder.decodeObject(forKey: "stepPointsDataToPrint") as! [String]
        self.THRESHOLD = aDecoder.decodeDouble(forKey: "THRESHOLD")
        self.TIMEDIFFERENCE = aDecoder.decodeDouble(forKey: "TIMEDIFFERENCE")
        self.previousStepPointTime = aDecoder.decodeDouble(forKey: "previousStepPointTime")
        self.pedometerStepCount = aDecoder.decodeInt64(forKey: "pedometerStepCount")
    }
    
    func calculateRelativeTimeAndYaw(){
        let startTime = sensorPoints[0].time
        var currentTime = 0.0
        var relativeTime = 0.0
        
        let startYaw = sensorPoints[0].attitudeYaw
        var currentYaw = 0.0
        var relativeYaw = 0.0
        
        for step in 0..<sensorPoints.count{
            currentTime = sensorPoints[step].time
            relativeTime = currentTime - startTime
            sensorPoints[step].relativeTime = relativeTime
            
            currentYaw = sensorPoints[step].attitudeYaw
            relativeYaw = currentYaw - startYaw
            sensorPoints[step].relativeYaw = relativeYaw
        }
    }
    
    func calculateSteps(){
        if sensorPoints.count >= 2{
            for i in 1..<sensorPoints.count-1{
                if sensorPoints[i-1].calculatedVertivalAcceleration < sensorPoints[i].calculatedVertivalAcceleration,
                    sensorPoints[i].calculatedVertivalAcceleration > sensorPoints[i+1].calculatedVertivalAcceleration,
                    sensorPoints[i].calculatedVertivalAcceleration > THRESHOLD,
                    sensorPoints[i].time - previousStepPointTime > TIMEDIFFERENCE
                {
                    stepPointsBetweenZeros.append(sensorPoints[i])
                    previousStepPointTime = sensorPoints[i].time
                }
                else if sensorPoints[i-1].calculatedVertivalAcceleration > sensorPoints[i].calculatedVertivalAcceleration,
                    sensorPoints[i].calculatedVertivalAcceleration < sensorPoints[i+1].calculatedVertivalAcceleration,
                    sensorPoints[i].calculatedVertivalAcceleration < 0.0
                {
                    if stepPointsBetweenZeros.count != 0{
                        var maxpoint = 0.0
                        var index = 0
                        for p in 0..<stepPointsBetweenZeros.count{
                            if stepPointsBetweenZeros[p].calculatedVertivalAcceleration >= maxpoint{
                                maxpoint = stepPointsBetweenZeros[p].calculatedVertivalAcceleration
                                index = p
                            }
                        }
                        calculatedStepPoints.append(stepPointsBetweenZeros[index])
                        stepPointsBetweenZeros.removeAll()
                    }
                }
                
            }
        }
    }
    
    func calculateAverageYawBetweenSteps(){
        var k = 0
        var pointsBetweenSteps = 0.0
        var averageYaw = 0.0
        for i in 0..<calculatedStepPoints.count{
            pointsBetweenSteps = 0.0
            averageYaw = 0.0
            while calculatedStepPoints[i].time != sensorPoints[k].time {
                averageYaw += sensorPoints[k].attitudeYaw
                pointsBetweenSteps += 1
                k += 1
            }
            averageYaw = averageYaw / pointsBetweenSteps
            calculatedStepPoints[i].averageYaw = averageYaw
        }
    }
    
    func createCellRecord(){
        var counter = 1
        for point in calculatedStepPoints{
            stepPointsDataToPrint.append("\(counter). time: \(round(1000*point.relativeTime)/1000)s - acceleration: \(round(1000*point.calculatedVertivalAcceleration)/1000) - yaw: \(round(1000 * point.averageYaw * (180.0/Double.pi))/1000)˚")
            counter += 1
        }
        stepPointsDataToPrint.reverse()
    }
}
