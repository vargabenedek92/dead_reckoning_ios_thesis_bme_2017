//
//  History.swift
//  DEAD_RECKONING_iOS_thesis_bme_2017
//
//  Created by Benedek Varga on 2017. 05. 01..
//  Copyright © 2017. Benedek Varga. All rights reserved.
//

import Foundation

class History: NSObject, NSCoding{
    var historyList: [DeadReckoningModel] =  []
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(self.historyList, forKey: "historyList")
    }
    
    required init?(coder aDecoder: NSCoder) {
        self.historyList = aDecoder.decodeObject(forKey: "historyList") as! [DeadReckoningModel]
    }
    
    override init() {
    }
}
