//
//  LoadedEntryViewController.swift
//  DEAD_RECKONING_iOS_thesis_bme_2017
//
//  Created by Benedek Varga on 2017. 05. 01..
//  Copyright © 2017. Benedek Varga. All rights reserved.
//

import UIKit

class LoadedEntryViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate {
    var loadedEntry = DeadReckoningModel()
    
    @IBOutlet weak var stepCountLabel: UILabel!
    @IBOutlet weak var pedometerCountLabel: UILabel!
    @IBOutlet weak var thresholdTextField: UITextField!
    @IBOutlet weak var entryTable: UITableView!
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationItem.title = loadedEntry.title
    }
    
    override func viewDidLoad() {
        stepCountLabel.text = "\(loadedEntry.stepPointsDataToPrint.count)"
        thresholdTextField.text = "\(loadedEntry.THRESHOLD)"
        pedometerCountLabel.text = "\(loadedEntry.pedometerStepCount)"
        entryTable.reloadData()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return loadedEntry.stepPointsDataToPrint.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: UITableViewCell = entryTable.dequeueReusableCell(withIdentifier: "entryCell")!
        cell.textLabel?.text = loadedEntry.stepPointsDataToPrint[indexPath.row]
        return cell
    }
    
    
    @IBAction func evaluateButtonPressed(_ sender: UIButton) {
        let evaluation = DeadReckoningModel()
        evaluation.sensorPoints = loadedEntry.sensorPoints
        evaluation.THRESHOLD = loadedEntry.THRESHOLD
        
        evaluation.calculateSteps()
        evaluation.calculateAverageYawBetweenSteps()
        evaluation.createCellRecord()
        
        stepCountLabel.text = "\(evaluation.stepPointsDataToPrint.count)"
        loadedEntry.stepPointsDataToPrint = evaluation.stepPointsDataToPrint
        entryTable.reloadData()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        thresholdTextField.resignFirstResponder()
        if thresholdTextField.text != ""{
            if let newThreshold = Double(thresholdTextField.text!){
                loadedEntry.THRESHOLD = newThreshold
            }
            else{
                let alert = UIAlertController(title: "Invalid Threshold", message: "Your threshold is set to \(String(loadedEntry.THRESHOLD)).", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
                thresholdTextField.text = String(loadedEntry.THRESHOLD)
            }
        }
        return true
    }
    
    @IBAction func buttonTouchAnimate(_ sender: UIButton) {
        UIView.animate(withDuration: 0.05, animations: {sender.transform = CGAffineTransform(scaleX: 0.95, y: 0.95)})
    }
    
    @IBAction func buttonReleaseAnimate(_ sender: UIButton) {
        UIView.animate(withDuration: 0.05, animations: {sender.transform = CGAffineTransform.identity})
    }
    
    
    
}
