//
//  Pedometer.swift
//  DEAD_RECKONING_iOS_thesis_bme_2017
//
//  Created by Benedek Varga on 2017. 05. 08..
//  Copyright © 2017. Benedek Varga. All rights reserved.
//

import Foundation
import CoreMotion

class Pedometer{
    var steps = 0
    
    var pedometer = CMPedometer()
    var startTime = Date()
    var stopTime = Date()
    
    func startPedometer() {
        startTime = Date()
        steps = 0
    }
    
    func stopPedometer(callback: @escaping ()->Void) {
        stopTime = Date()
        pedometer.queryPedometerData(from: startTime, to: stopTime, withHandler: { (pedometerData, error) in
                if let pedData = pedometerData{
                    self.steps = Int(pedData.numberOfSteps)
                }
                callback()
            })
    }
}
