//
//  HistoryViewController.swift
//  DEAD_RECKONING_iOS_thesis_bme_2017
//
//  Created by Benedek Varga on 2017. 05. 01..
//  Copyright © 2017. Benedek Varga. All rights reserved.
//

import UIKit

class HistoryViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    var history = History()
    
    @IBOutlet weak var tableViewForHistory: UITableView!
    
    override func viewDidLoad() {
        tableViewForHistory.reloadData()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return history.historyList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: UITableViewCell = tableViewForHistory.dequeueReusableCell(withIdentifier: "historyCell")!
        cell.textLabel?.text = history.historyList[indexPath.row].title
        return cell
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            history.historyList.remove(at: indexPath.row)
            
            let historyDefaults = UserDefaults.standard
            let historyData: Data = NSKeyedArchiver.archivedData(withRootObject: self.history.historyList)
            historyDefaults.setValue(historyData, forKey: "History")
            
            tableViewForHistory.reloadData()
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let destinationViewController = segue.destination
        if let loadedEntryViewController = destinationViewController as? LoadedEntryViewController{
            if segue.identifier == "loadEntry"{
                loadedEntryViewController.loadedEntry = history.historyList[(tableViewForHistory.indexPathForSelectedRow?.row)!]
            }
        }
    }
}
