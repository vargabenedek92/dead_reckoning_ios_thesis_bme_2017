//
//  SensorDataPoint.swift
//  DEAD_RECKONING_iOS_thesis_bme_2017
//
//  Created by Benedek Varga on 2017. 04. 29..
//  Copyright © 2017. Benedek Varga. All rights reserved.
//

import Foundation

class SensorDataPoint: NSObject, NSCoding {
    //    required data:
    //    private Double time;
    //    private Double userAccelerationY;
    //    private Double userAccelerationZ;
    //    private Double gravityY;
    //    private Double gravityZ;
    //    private Double attitudeYawRAD;
    //    private Double calcVertAcc;
    
    var time: Double
    var relativeTime: Double
    var accelerationY: Double
    var accelerationZ: Double
    var gravityY: Double
    var gravityZ: Double
    var attitudeYaw: Double
    var relativeYaw: Double
    var averageYaw: Double
    var calculatedVertivalAcceleration: Double
    
    init(at time: Double, accY aY: Double, accZ aZ: Double, gravY gY: Double, gravZ gZ: Double, yaw attY: Double){
        self.time = time
        self.relativeTime = 0.0
        self.accelerationY = aY
        self.accelerationZ = aZ
        self.gravityY = gY
        self.gravityZ = gZ
        
        //fixing YAW data
        if attY < 0.0{
            self.attitudeYaw = attY + 2 * Double.pi
        }
        else{
            self.attitudeYaw = attY
        }
        self.relativeYaw = 0.0
        self.averageYaw = 0.0
        //compensating phone Y tilt
        self.calculatedVertivalAcceleration = (aZ*sin(atan(abs(gZ/gY)))) + (aY*cos(atan(abs(gZ/gY))))
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(self.time, forKey: "time")
        aCoder.encode(self.relativeTime, forKey: "relativeTime")
        aCoder.encode(self.accelerationY, forKey: "accelerationY")
        aCoder.encode(self.accelerationZ, forKey: "accelerationZ")
        aCoder.encode(self.gravityY, forKey: "gravityY")
        aCoder.encode(self.gravityZ, forKey: "gravityZ")
        aCoder.encode(self.attitudeYaw, forKey: "attitudeYaw")
        aCoder.encode(self.relativeYaw, forKey: "relativeYaw")
        aCoder.encode(self.averageYaw, forKey: "averageYaw")
        aCoder.encode(self.calculatedVertivalAcceleration, forKey: "calculatedVertivalAcceleration")
    }
    
    required init?(coder aDecoder: NSCoder) {
        self.time = aDecoder.decodeDouble(forKey: "time")
        self.relativeTime = aDecoder.decodeDouble(forKey: "relativeTime")
        self.accelerationY = aDecoder.decodeDouble(forKey: "accelerationY")
        self.accelerationZ = aDecoder.decodeDouble(forKey: "accelerationZ")
        self.gravityY = aDecoder.decodeDouble(forKey: "gravityY")
        self.gravityZ = aDecoder.decodeDouble(forKey: "gravityZ")
        self.attitudeYaw = aDecoder.decodeDouble(forKey: "attitudeYaw")
        self.relativeYaw = aDecoder.decodeDouble(forKey: "relativeYaw")
        self.averageYaw = aDecoder.decodeDouble(forKey: "averageYaw")
        self.calculatedVertivalAcceleration = aDecoder.decodeDouble(forKey: "calculatedVertivalAcceleration")
    }
}
