//
//  ViewController.swift
//  DEAD_RECKONING_iOS_thesis_bme_2017
//
//  Created by Benedek Varga on 2017. 04. 29..
//  Copyright © 2017. Benedek Varga. All rights reserved.
//

import UIKit
import CoreMotion
import HealthKit

class DeadReckoningViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate {
    let motionManager = CMMotionManager()
    var timer: Timer!
    var dataIsBeingRecorded = false
    var dataWasRecorded = false{
        didSet{
            if dataWasRecorded == true{
                reevaluateButton.isEnabled = true
                reevaluateButton.backgroundColor = UIColor(colorLiteralRed: 52/255, green: 73/255, blue: 94/255, alpha: 1)
                
                saveButton.isEnabled = true
                saveButton.backgroundColor = UIColor(colorLiteralRed: 52/255, green: 73/255, blue: 94/255, alpha: 1)
            }
            else{
                reevaluateButton.isEnabled = false
                reevaluateButton.backgroundColor = UIColor(colorLiteralRed: 147/255, green: 153/255, blue: 158/255, alpha: 1)
                
                saveButton.isEnabled = false
                saveButton.backgroundColor = UIColor(colorLiteralRed: 147/255, green: 153/255, blue: 158/255, alpha: 1)
            }
        }
    }
    
    private var deadReckoningModel = DeadReckoningModel()
    
    private var pedometer = Pedometer()
    
    private var history = History()
    
    @IBOutlet weak var stepCounterLabel: UILabel!
    
    @IBOutlet weak var pedometerStepCount: UILabel!
    
    @IBOutlet weak var thresholdTextField: UITextField!
    
    @IBOutlet weak var dynamicThresholdSwitch: UISwitch!
    
    @IBOutlet weak var heigthTextField: UITextField!
    
    @IBOutlet weak var genderSegmentedPicker: UISegmentedControl!
    
    @IBOutlet weak var tableForDatapoints: UITableView!
    
    @IBOutlet weak var reevaluateButton: UIButton!
    
    @IBOutlet weak var saveButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableForDatapoints.delegate = self
        tableForDatapoints.dataSource = self
        thresholdTextField.text = String(deadReckoningModel.THRESHOLD)
        motionManager.startDeviceMotionUpdates()
        
        let historyDefaults = UserDefaults.standard
        if historyDefaults.value(forKey: "History") != nil{
            let historyData = historyDefaults.object(forKey: "History") as! Data
            history.historyList = NSKeyedUnarchiver.unarchiveObject(with: historyData) as! [DeadReckoningModel]
        }
        
        
        //50Hz
        timer = Timer.scheduledTimer(timeInterval: 0.02, target: self, selector: #selector(DeadReckoningViewController.recordSensorData), userInfo: nil, repeats: true)
    }
    
    func recordSensorData() {
        if let deviceMotion = motionManager.deviceMotion, dataIsBeingRecorded {
            let sensorDataPoint = SensorDataPoint(at: Double(Date.timeIntervalSinceReferenceDate),
                                                  accY: deviceMotion.userAcceleration.y,
                                                  accZ: deviceMotion.userAcceleration.z,
                                                  gravY: deviceMotion.gravity.y,
                                                  gravZ: deviceMotion.gravity.z,
                                                  yaw: deviceMotion.attitude.yaw)
            deadReckoningModel.sensorPoints.append(sensorDataPoint)
        }
    }
    
    func readPedometer(){
        
    }
    
    @IBAction func startStopRecording(_ sender: UIButton) {
        dataIsBeingRecorded = !dataIsBeingRecorded
        //START
        if dataIsBeingRecorded{
            sender.setTitle("Stop✋", for: .normal)
            sender.backgroundColor = UIColor(colorLiteralRed: 183/255, green: 54/255, blue: 51/255, alpha: 1)
            stepCounterLabel.text = "0"
            pedometerStepCount.text = "0"

            dataWasRecorded = false

            pedometer.startPedometer()
            
            deadReckoningModel.sensorPoints.removeAll()
            deadReckoningModel.calculatedStepPoints.removeAll()
            deadReckoningModel.stepPointsDataToPrint.removeAll()
            
            tableForDatapoints.reloadData()
        }
        //STOP
        else{
            sender.setTitle("Start🚀", for: .normal)
            sender.backgroundColor = UIColor(colorLiteralRed: 93/255, green: 151/255, blue: 113/255, alpha: 1)
            dataWasRecorded = true
            
            pedometer.stopPedometer{
                self.pedometerStepCount.text = "\(self.pedometer.steps)"
                self.deadReckoningModel.pedometerStepCount = Int64(self.pedometer.steps)
            }
            
            deadReckoningModel.calculateRelativeTimeAndYaw()
            deadReckoningModel.calculateSteps()
            deadReckoningModel.calculateAverageYawBetweenSteps()
            deadReckoningModel.createCellRecord()
            
            stepCounterLabel.text = "\(deadReckoningModel.stepPointsDataToPrint.count)"
            tableForDatapoints.reloadData()
        }
    }
    
    @IBAction func reevaluateRecordedData(_ sender: UIButton) {
        let newDeadReckoningModel = DeadReckoningModel()
        newDeadReckoningModel.sensorPoints = deadReckoningModel.sensorPoints
        newDeadReckoningModel.THRESHOLD = deadReckoningModel.THRESHOLD
        
        newDeadReckoningModel.calculateSteps()
        newDeadReckoningModel.calculateAverageYawBetweenSteps()
        newDeadReckoningModel.createCellRecord()
        
        stepCounterLabel.text = "\(newDeadReckoningModel.stepPointsDataToPrint.count)"
        deadReckoningModel.sensorPoints = newDeadReckoningModel.sensorPoints
        deadReckoningModel.THRESHOLD = newDeadReckoningModel.THRESHOLD
        deadReckoningModel.calculatedStepPoints = newDeadReckoningModel.calculatedStepPoints
        deadReckoningModel.stepPointsDataToPrint = newDeadReckoningModel.stepPointsDataToPrint
        tableForDatapoints.reloadData()
    }
    
    @IBAction func saveRecordedData(_ sender: UIButton) {
        let alert = UIAlertController(title: "Save Data", message: "Add title to your entry.", preferredStyle: .alert)
        
        let newEntry = DeadReckoningModel()
        newEntry.sensorPoints = deadReckoningModel.sensorPoints
        newEntry.calculatedStepPoints = deadReckoningModel.calculatedStepPoints
        newEntry.stepPointsDataToPrint = deadReckoningModel.stepPointsDataToPrint
        newEntry.THRESHOLD = deadReckoningModel.THRESHOLD
        newEntry.title = deadReckoningModel.title
        newEntry.isThresholdDynamic = deadReckoningModel.isThresholdDynamic
        newEntry.isUserMale = deadReckoningModel.isUserMale
        newEntry.userHeigth = deadReckoningModel.userHeigth
        newEntry.pedometerStepCount = deadReckoningModel.pedometerStepCount
        
        alert.addTextField { (textField) in
            textField.text = ""
            textField.keyboardType = .alphabet
            textField.autocapitalizationType = .sentences
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel)
        alert.addAction(cancelAction)
        
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { [weak alert] (_) in
            let currentdate = Date()
            let formatter = DateFormatter()
            formatter.dateFormat = "ddMMyyyy_HHmmss"
            let convertedDate = formatter.string(from: currentdate)
            
            let textField = alert?.textFields![0]
            var title = textField?.text
            if title! == ""{
                title = "new data"
            }

            let entryTitle = "\(title!)_\(convertedDate)"
            newEntry.title = entryTitle
            self.history.historyList.append(newEntry)
            
            let historyDefaults = UserDefaults.standard
            let historyData: Data = NSKeyedArchiver.archivedData(withRootObject: self.history.historyList)
            historyDefaults.setValue(historyData, forKey: "History")
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return deadReckoningModel.stepPointsDataToPrint.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: UITableViewCell = tableForDatapoints.dequeueReusableCell(withIdentifier: "tableCell")!
        cell.textLabel?.text = deadReckoningModel.stepPointsDataToPrint[(indexPath as NSIndexPath).row]
        return cell
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        thresholdTextField.resignFirstResponder()
        if thresholdTextField.text != ""{
            if let newThreshold = Double(thresholdTextField.text!){
                deadReckoningModel.THRESHOLD = newThreshold
            }
            else{
                let alert = UIAlertController(title: "Invalid Threshold", message: "Your threshold is set to \(String(deadReckoningModel.THRESHOLD)).", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
                thresholdTextField.text = String(deadReckoningModel.THRESHOLD)
            }
        }
        return true
    }
    
    @IBAction func buttonTouchAnimate(_ sender: UIButton) {
        UIView.animate(withDuration: 0.05, animations: {sender.transform = CGAffineTransform(scaleX: 0.95, y: 0.95)})
    }
    @IBAction func buttonReleaseAnimate(_ sender: UIButton) {
        UIView.animate(withDuration: 0.05, animations: {sender.transform = CGAffineTransform.identity})
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let destinationViewController = segue.destination
        if let historyViewController = destinationViewController as? HistoryViewController{
            if segue.identifier == "toHistory"{
                historyViewController.history = history
            }
        }
    }
}
